Title: Игра "Жизнь"
Date: 2018-01-01 21:57
Category: Code
Slug: react-life-game

После длительного перерыва было решено снова возвращаться к работе. Игра "Жизнь" - мой первый проект на React.

![Игра Жизнь](img/life.png)

[Играть](https://lutretyakova.gitlab.io/projects/react-life-game/)