Title:Пиксельный редактор
Date: 2018-07-18 15:43
Category: Code
Slug: react-pixel-editor

Следующий проект для изучения возможностей React+Redux - пиксельный редактор.
![PixelEditor](img/pixeleditor/img1.png)

В пиксельном редакторе есть следующие инструменты для рисования:

 - карадаш;
 - ластик;
 - прямоугольник;
 - круг;
 - прямая линия;
 - заливка;
 - выбор цвета.

А также дополнительные возможности:

   - предпросмотр;
   - сохранение изображения на компьютере пользователя.

Размер области для рисования - 25 х 28 пикселей.

Область рисования редактора первоначально была реализована путем добавления сетки из
div-компонентов, где каждый div - представлял собой один пиксель будущего изображения. 
От этого способа пришлось отказаться, т.к. при рисовании прямоугольника стала 
заметна задержка рисования. Чтобы найти проблему, я измерила время обработки каждого 
события с помощью [React Developer Tools](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=0ahUKEwiZ-62srqncAhWSZpoKHe47BYoQFgg7MAM&url=https%3A%2F%2Fchrome.google.com%2Fwebstore%2Fdetail%2Freact-developer-tools%2Ffmkadmapgofadopljbjfkapdkoienihi&usg=AOvVaw3YJDg7kXgeeChgKN88s0Sx).

Оказалось, что одно событиие mousemove и связанная с ним пачка событий выполняются примерно 311ms, что довольно много. При этом 
290ms осуществляется обновление и перерисовка компонента Canvas (который на самом деле div) и его дочерних компонентов Cell (тех самых div-пикселей).
![PixelEditor](img/pixeleditor/img2.png)

После перехода к обрасти рисования на канве (canvas), скорость отрисовки графических примитивов 
возросла в несколько раз. Это отражено и при замере скорости в React Developer Tools.

![PixelEditor](img/pixeleditor/img3.png)
Сетка канвы для визуализации пикселей выполнена в CSS. Размер в backgroundSize - размер
ячейки.

```javascript
    const canvasStyle = {
        backgroundSize: `${CELL_WIDTH}px ${CELL_WIDTH}px`
    }
```
```css
  background: linear-gradient(to right, #464646 1px, transparent 1px), 
                    linear-gradient(to bottom, #464646 1px, transparent 1px);
```

Для рисования графических примитивов реализованы следующие алгоритмы:

- [алгоритм Брезенхема](https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%91%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%85%D1%8D%D0%BC%D0%B0) для рисования прямой линии;
- [Midpoint алгоритм](https://en.wikipedia.org/wiki/Midpoint_circle_algorithm) для рисования окружностей и эллипсов.

Кусок кода для расчета координат прямой линии по алгоритму Брезенхема:
```javascript
    ...
    if (Math.abs(startX - endX) > Math.abs(startY - endY)) {
        const angularRatio = Math.abs((endY - startY) / (endX - startX));
        const xCoords = getCoordsOffset(startX, endX);
        const yCoords = bresenhemLine(startY, endY, angularRatio, xCoords.length);

        return filterCanvasBorder(
                    yCoords.map((item, inx) => ([item, xCoords[inx]])))
    ...
   
    const bresenhemLine = (start, end, angularRatio, len) => {
    let err = 0;
    let coord = start;
    return [start].concat(
        Array(len - 1)
            .fill()
            .map((_) => {
                err += angularRatio;
                if (err >= 0.5) {
                    coord += (end - start) > 0 && (end - start) !== 0 ? 1 : -1;
                    err -= 1;
                }
                return coord
            })
    )
}
```

Нарисованное изображение сохраняется на компьютере пользователя в формате PNG.

![PixelEditor](img/pixeleditor/img4.png)


[Ссылка на репозиторий](https://gitlab.com/lutretyakova/react-pixel-editor)

[Хотите порисовать?](https://lutretyakova.gitlab.io/projects/react-pixeleditor/)
 