Title: Игра "Сокобан"
Date: 2018-07-30 12:43
Category: Code
Slug: react-sokoban

Следующий проект - игра "Сокобан". 
Сокобан — логическая игра-головоломка, в которой игрок передвигает ящики по лабиринту, показанному в виде плана, 
с целью поставить все ящики на заданные конечные позиции. Только один ящик может быть передвинут за раз, 
причём герой игры — «кладовщик» — может только толкать ящики, но не тянуть их. 

![Сокобан](img/sokoban/sokoban.png)

Реализация игры имеет несколько особенностей:

- карта хранится в json-файле. Таким образом, при изменении файла можно менять игровое поле. Это пригодится
 для следующего проекта - редактора уровней для Сокобана.

- игровое поле реализовано на канве. Это упростило отображение игрового поля и перемещение объекта-кладовщика.
 Для загрузки изображений, я использовала механизм Promise. 

```javascript
this.imagesPromises = {
    [WALL]: this.loadImage(wallImg),
    ...
}
loadImage(url){
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.addEventListener('load', e => resolve(img));
        img.addEventListener('error', () => {
            reject(new Error(`Failed to load image's URL: ${url}`));
        });
        img.src = url;
    });
}

    ...
    this.imagesPromises[state.board[y][x]].then(img => {
        ctx.drawImage(img, x * SIZE_BLOCK, y * SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK)
    })
    ...
```

- в CSS выполнен красивый визуальный эффект при установке ящика на место.

```css
    .animation {
        color: rgba(255, 255, 255, 0.3);
        font-size: 25em;
        font-weight: bold;
        z-index: 5;
        animation: ani 0.8s forwards;
    }

    @keyframes ani {
        30% {
            opacity: 1
        }
        50% {
            opacity: 0.5
        }
        100% {
            opacity: 0
        }
    }
```


[Ссылка на репозиторий](https://gitlab.com/lutretyakova/react-sokoban)

[Поиграть](https://lutretyakova.gitlab.io/projects/react-sokoban/)