Title: LuReader - читалка электронных книг в формате fb2
Date: 2019-03-16 21:14
Category: Code
Slug: react-lureader

С последнего проекта на React прошло 4 месяца.
В это время помимо разработки на React, я решала задачи для челленджей 100 Days of CSS
и на codepen.io. Решения можно посмотреть в моём 
профиле [https://codepen.io/ludmila-tretyakova/](https://codepen.io/ludmila-tretyakova/) на codepen.io.
На данный момент там чуть больше 50 мини-проектов на JavaScript, CSS и немного SVG.

На React-е с ноября разрабатывала проект LuReader. LuReader - читалка для электронных книг в формате fb2.
Я люблю читать электронные книги. Мне стало интересно разобраться в формате электронных книг и написать свою версию читалки.
Как мне кажется, у меня получилась довольно удобная читалка с лаконичным дизайном.

Скрин проекта - это ссылка на gif-ку с процессом работы с читалкой.

[![Читалка электронных книг в формате .fb2](img/lureader/img1.png)](img/lureader/reader.gif)

Что читалка может делать:

 - загружать файлы в формате fb;
 - отображать текст книги с учетом разрешения экрана;
 - перелистывание страниц вперёд и назад;
 - показывать информацию о книге: автор, название, картинку обложки, дата издания, аннотацию, переводчик и т.п.
 - показывать прогресс чтения данной книги;
 - отображать список уже прочитанных книг.

Что делала и чему научилась во время работы над проектом:

 - Для того чтобы правильно листать страницы, нужно посчитать сколько слов в ней помещается. 
 Тогда конец текста на одной странице - начало на следующей. Чтобы правильно расчитать эти размеры
 с учетом разрешения монитора, придумала несколько вариантов решения этой проблемы.
 - Научилась работать с IndexedDB: чтение, запись и обновление данных. 
 - Разработала модель базы данных для приложения с учетом того, что IndexedDB не позволяет строить связи один-ко-многим.
 - Разработала парсеры сырых данных из файла для получения данных из книги.
 - Написала регулярные выражения к парсерам.
 - Сделала резиновую верстку проекта для разных разрешений экрана.

 [Читать книгу](https://lutretyakova.gitlab.io/projects/lureader/)

