Title: BallStick
Date: 2019-11-05 22:17
Category: Code
Slug: android-ballstick

Недавно в рамках челленджа на codepen/io я написала прототив игрушки 
[https://codepen.io/ludmila-tretyakova/pen/oNNXVVj](https://codepen.io/ludmila-tretyakova/pen/oNNXVVj)

Идея мне понравилась, и я решила сделать полноценную игру на Kotlin & Android. Получилась динамичная и интересная игра. 

![BallStick](img/android-ballstick/promo.png)

BallStick - это интересная игра на развитие внимания для взрослых и детей.  
В игре шары двух цветов — голубой и оранжевый. Шар каждого цвета нужно отправить к линии своего цвета, нажав на 
нужную половину экрана. На первый взгляд, это простая игра, но для достижения наилучшего результата нужно быть 
сосредоточенным и внимательным. Игра предназначена для телефонов и планшетов и работает в портретной ориентации экрана.

[BallStick](https://play.google.com/store/apps/details?id=com.rizhiylis.sorting)