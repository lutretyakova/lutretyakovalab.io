Title: Редактор уровней игры "Сокобан" и игра "Сокобан" с загрузкой уровней
Date: 2018-08-27 13:49
Category: Code
Slug: react-sokoban-editor

Следующий проект - это связка двух проектов: редактор уровней игры "Сокобан" и игра "Сокобан".

![Редактор уровней игры "Сокобан"](img/sokobaneditor/sokoban1.png)

В игре пользователь загружает созданный в редакторе уровень и играет в игру.
Доступ к уровню, который создает пользователь, должен предоставляться отовюду. Значит, нужна
удаленная база, которая позволяет работать с необходимыми данными.

MongoDB Atlas прекрасно подошел для целей проекта, потому что дает:

- удаленный доступ к базе MongoDB любому пользователю;

- возможность поработать с Serverless технологией;

- простую работу с данными;

- хорошую документацию.

В рамках моего проекта MongoDB Atlas позволил:

- сохранить новый уровень в таблицу;

- отредактировать текущий уровень;

- загрузить список всех созданных уровней.

Также в редакторе уровней было интересно сделать непосредственно редактор.
Отрисовка панели инструментов, выбор компонентов для добавления на игровое поле, их 
перетаскивание мышью, привязка компонентов к сетке игрового поля при перемещении
полностью реализовано на Canvas. 

Когда редактор уровней был готов, я использовала часть кода с предыдущего проекта игры "Сокобан" и с текущего 
редактора и сделала новую версию игры "Сокобан". В этом проекте отредактированные уровни загружаются с 
удаленного сервера для дальнейшей игры.

![Игра "Сокобан"](img/sokobaneditor/sokoban2.png)

[Ссылка на репозиторий Редактора уровней](https://gitlab.com/lutretyakova/react-sokoban-editor)

[Сделать свой уровень](https://lutretyakova.gitlab.io/projects/react-sokoban-editor)

[Ссылка на репозиторий игрушки](https://gitlab.com/lutretyakova/react-sokoban-with-levels)

[Поиграть](https://lutretyakova.gitlab.io/projects/react-sokoban-with-levels)